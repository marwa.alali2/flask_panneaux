import os
from flask import Flask, request, render_template
import label_image

app = Flask(__name__)
signDict= {"0":"Cassis ou dos-d'âne",
"1":"Ralentisseur de type dos-d'âne",
"2":"Chaussée particulièrement glissante",
"3":"Virage à gauche",
"4":"Virage à droite",
"5":"Succession de virages dont le premier est à gauche",
"6":"Succession de virages dont le premier est à droite",
"7":"Endroit fréquenté par les enfants",
"8":"Débouché de cyclistes venant de droite ou de gauche",
"9":"Passage d'animaux domestiques",
"10":"Travaux",
"11":"Annonce de feux tricolores",
"12":"Passage à niveau",
"13":"Autres dangers",
"14":"Chaussée rétrécie",
"15":"Chaussée rétrécie par la gauche",
"16":"Chaussée rétrécie par la droite",
"17":"Intersection avec une route avec passage prioritaire",
"18":"priorité à droite",
"19":"Cédez le passage ",
"20":"Cédez le passage à la circulation venant en sens inverse",
"21":"STOP",
"22":"Sens interdit à tout véhicule",
"23":"Accès interdit aux cycles",
"24":"Accès interdit aux véhicules  dont le poids total excède le nombre indiqué",
"25":"Accès interdit aux véhicules affectés au transport de marchandises",
"26":"Accès interdit aux véhicules dont la largeur",
"27":"Accès interdit aux véhicules dont la hauteur",
"28":"Circulation interdite à tout véhicule dans les deux sens",
"29":"Interdiction de tourner à gauche à la prochaine intersection",
"30":"Interdiction de tourner à droite à la prochaine intersection",
"31":"Dépassement interdit",
"32":"Limitation de vitesse",
"33":"Chemin obligatoire pour piéton et cycliste",
"34":"Direction obligatoire toute droite",
"35":"Direction obligatoire  à gauche",
"36":"Direction obligatoire tout droit ou à droite",
"37":"Rond point",
"38":"Piste ou bande obligatoire pour les cycles",
"39":"Chemin obligatoire pour piéton et cycliste",
"40":"Stationnement interdit",
"41":"Arrêt et stationnement interdits",
"42":"Stationnement interdit du 1er au 15 du mois",
"43":"Stationnement interdit du 16 à la fin du mois",
"44":"Priorité par rapport à la circulation venant en sens inverse",
"45":"Lieu aménagé pour le stationnement",
"46":"Lieu aménagé pour le stationnement",
"47":"Lieu aménagé pour le stationnement",
"48":"Lieu aménagé pour le stationnement",
"49":"Lieu aménagé pour le stationnement",
"50":"Lieu aménagé pour le stationnement",
"51":"51",
"52":"52",
"53":"Circulation à sens unique",
"54":"Impasse",
"55":"55",
"56":"Passage pour piétons",
"57":"Piste ou bande cyclable conseillée",
"58":"Parking",
"59":"Surélévation de chaussée",
"60":"Fin du caractère prioritaire d'une route",
"61":"Indication du caractère prioritaire d'une route"} #45: ...gratuit à durée limitée avec contrôle par disque

@app.route('/', methods = ['GET', 'POST'])
def hello_world():
    if request.method == 'GET':
       return render_template('index.html')
    if request.method == 'POST':
        if 'file' not in request.files:
            print('file not uploaded')
            return
        file = request.files['file']
        image = file.read()
        result ,prediction = label_image.get_predictions(image)
        filename = file.filename
        print(prediction)
       
        top1 = "1. {}: {} %".format(signDict[str(result[-1])],round( prediction[0][result[-1]]*100, 2))
        top2 = "2. {}: {} %".format(signDict[str(result[-2])], round(prediction[0][result[-2]]*100, 2))
        top3 = "3. {}: {} %".format(signDict[str(result[-3])], round(prediction[0][result[-3]]*100, 2))

        return render_template('result.html', output_top1=top1, output_top2=top2, output_top3=top3, image=filename)

if __name__ == '__main__':
    app.run(debug=True)

